import { EventEmitter } from 'eventemitter3';
import { MessageId, v1, Version } from 'rp-chat-types';

import { RPChatsAPIv1 } from './RPChatsAPIv1';

interface Params {
    messageId: MessageId
}

interface EventMap {
    'error': (error: Error) => void;

    'v1/internal-error': (error: string, params: Params) => void;

    'v1/chats': (chats: v1.IChat[], params: Params) => void;
    'v1/chats/describe': (chat: v1.IChatDetails, params: Params) => void;
    'v1/chats/new': (chat: v1.IChat, params: Params) => void;
    'v1/chats/edit': (chat: v1.IChat, params: Params) => void;
    'v1/chats/delete': (chat: v1.IChat, params: Params) => void;

    'v1/channels': (channels: v1.IChannel[], params: Params) => void;
    'v1/channels/describe': (channel: v1.IChannelDetails, params: Params) => void;
    'v1/channels/members': (members: v1.IChannelMember[], params: Params) => void;
    'v1/channels/new': (channel: v1.IChannel, params: Params) => void;
    'v1/channels/edit': (channel: v1.IChannel, params: Params) => void;
    'v1/channels/delete': (channel: v1.IChannel, params: Params) => void;

    'v1/messages': (messages: v1.IMessage[], page: number, params: Params) => void;
    'v1/messages/new': (message: v1.IMessage, params: Params) => void;
    'v1/messages/edit': (message: v1.IMessage, params: Params) => void;
    'v1/messages/delete': (message: v1.IMessage, params: Params) => void;
}

interface IConfig {
    host: string;
    port: number;
    token: string;
    userId: number;
}

class RPChatsAPI extends EventEmitter<EventMap> {
    private _rpChatsAPIv1: RPChatsAPIv1;

    constructor(private readonly config: IConfig) {
        super();

        this._rpChatsAPIv1 = new RPChatsAPIv1({
            ...this.config,
            namespace: 'chats/v1'
        });

        this._rpChatsAPIv1.on('internal-error', (error: string, params: Params) => this.emit('v1/internal-error', error, params));

        this._rpChatsAPIv1.on('chats', (chats: v1.IChat[], params: Params) => this.emit('v1/chats', chats, params));
        this._rpChatsAPIv1.on('chats/describe', (chat: v1.IChatDetails, params: Params) => this.emit('v1/chats/describe', chat, params));
        this._rpChatsAPIv1.on('chats/new', (chat: v1.IChat, params: Params) => this.emit('v1/chats/new', chat, params));
        this._rpChatsAPIv1.on('chats/edit', (chat: v1.IChat, params: Params) => this.emit('v1/chats/edit', chat, params));
        this._rpChatsAPIv1.on('chats/delete', (chat: v1.IChat, params: Params) => this.emit('v1/chats/delete', chat, params));

        this._rpChatsAPIv1.on('channels', (channels: v1.IChannel[], params: Params) => this.emit('v1/channels', channels, params));
        this._rpChatsAPIv1.on('channels/describe', (channel: v1.IChannelDetails, params: Params) => this.emit('v1/channels/describe', channel, params));
        this._rpChatsAPIv1.on('channels/members', (members: v1.IChannelMember[], params: Params) => this.emit('v1/channels/members', members, params));
        this._rpChatsAPIv1.on('channels/new', (channel: v1.IChannel, params: Params) => this.emit('v1/channels/new', channel, params));
        this._rpChatsAPIv1.on('channels/edit', (channel: v1.IChannel, params: Params) => this.emit('v1/channels/edit', channel, params));
        this._rpChatsAPIv1.on('channels/delete', (channel: v1.IChannel, params: Params) => this.emit('v1/channels/delete', channel, params));

        this._rpChatsAPIv1.on('messages', (messages: v1.IMessage[], page: number, params: Params) => this.emit('v1/messages', messages, page, params));
        this._rpChatsAPIv1.on('messages/new', (message: v1.IMessage, params: Params) => this.emit('v1/messages/new', message, params));
        this._rpChatsAPIv1.on('messages/edit', (message: v1.IMessage, params: Params) => this.emit('v1/messages/edit', message, params));
        this._rpChatsAPIv1.on('messages/delete', (message: v1.IMessage, params: Params) => this.emit('v1/messages/delete', message, params));
    }

    async init() {
        await this._rpChatsAPIv1.init();
    }

    async deinit() {
        try {
            await this._rpChatsAPIv1.deinit();
        } catch (error) {
            this.emit('error', error);
        }
    }

    chats({ version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.chats();
            default:
                return null;
        }
    }

    chatsDescribe(chatId: number, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.chatsDescribe(chatId);
            default:
                return null;
        }
    }

    chatsNew(newChat: v1.INewChat, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.chatsNew(newChat);
            default:
                return null;
        }
    }

    chatsEdit(editChat: v1.IEditChat, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.chatsEdit(editChat);
            default:
                return null;
        }
    }

    chatsDelete(deleteChat: v1.IDeleteChat, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.chatsDelete(deleteChat);
            default:
                return null;
        }
    }

    channels({ version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channels();
            default:
                return null;
        }
    }

    channelsDescribe(channelId: number, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channelsDescribe(channelId);
            default:
                return null;
        }
    }

    channelsMembers(channelId: number, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channelsMembers(channelId);
            default:
                return null;
        }
    }

    channelsMembersEdit(channelId: number, membersToAdd: number[], membersToRemove: number[], { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channelsMembersEdit(channelId, membersToAdd, membersToRemove);
            default:
                return null;
        }
    }

    channelsNew(newChannel: v1.INewChannel, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channelsNew(newChannel);
            default:
                return null;
        }
    }

    channelsEdit(editChannel: v1.IEditChannel, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channelsEdit(editChannel);
            default:
                return null;
        }
    }

    channelsDelete(deleteChannel: v1.IDeleteChannel, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.channelsDelete(deleteChannel);
            default:
                return null;
        }
    }

    messages(request: { channelId: number, page: number }, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.messages(request);
            default:
                return null;
        }
    }

    messagesNew(newMessage: v1.INewMessage, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.messagesNew(newMessage);
            default:
                return null;
        }
    }

    messagesEdit(editChannel: v1.IEditMessage, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.messagesEdit(editChannel);
            default:
                return null;
        }
    }

    messagesDelete(deleteChannel: v1.IDeleteMessage, { version }: { version: Version } = { version: 'v1' }) {
        switch (version) {
            case 'v1':
                return this._rpChatsAPIv1.messagesDelete(deleteChannel);
            default:
                return null;
        }
    }
}

export { RPChatsAPI };
