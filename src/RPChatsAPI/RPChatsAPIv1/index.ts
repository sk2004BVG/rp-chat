import { EventEmitter } from 'eventemitter3';
import io, { Socket } from 'socket.io-client';
import { v4 as uuid } from 'uuid';

import { MessageId, v1 } from 'rp-chat-types';

interface Params {
    messageId: MessageId
}

interface ResponseMap {
    'error': (error: Error) => void;

    'initialized': () => void;
    'internal-error': (error: string, params: Params) => boolean;

    'chats': (chats: v1.IChat[], params: Params) => boolean;
    'chats/describe': (chat: v1.IChatDetails, params: Params) => boolean;
    'chats/new': (chat: v1.IChat, params: Params) => boolean;
    'chats/edit': (chat: v1.IChat, params: Params) => boolean;
    'chats/delete': (chat: v1.IChat, params: Params) => boolean;

    'channels': (channels: v1.IChannel[], params: Params) => boolean;
    'channels/describe': (channel: v1.IChannelDetails, params: Params) => boolean;
    'channels/members': (members: v1.IChannelMember[], params: Params) => boolean;
    'channels/new': (channel: v1.IChannel, params: Params) => boolean;
    'channels/edit': (channel: v1.IChannel, params: Params) => boolean;
    'channels/delete': (channel: v1.IChannel, params: Params) => boolean;

    'messages': (messages: v1.IMessage[], page: number, params: Params) => boolean;
    'messages/new': (message: v1.IMessage, params: Params) => boolean;
    'messages/edit': (message: v1.IMessage, params: Params) => boolean;
    'messages/delete': (message: v1.IMessage, params: Params) => boolean;
}

interface RequestMap {
    'internal-error': (error: string, params: Params) => void;

    'chats': (params: Params) => void;
    'chats/describe': (chatId: number, params: Params) => void;
    'chats/new': (chat: v1.INewChat, params: Params) => void;
    'chats/edit': (chat: v1.IEditChat, params: Params) => void;
    'chats/delete': (chat: v1.IDeleteChat, params: Params) => void;

    'channels': (params: Params) => void;
    'channels/describe': (channelId: number, params: Params) => void;
    'channels/members': (channelId: number, params: Params) => void;
    'channels/members/edit': (channelId: number, membersToAdd: number[], membersToRemove: number[], params: Params) => boolean;
    'channels/new': (channel: v1.INewChannel, params: Params) => void;
    'channels/edit': (channel: v1.IEditChannel, params: Params) => void;
    'channels/delete': (channel: v1.IDeleteChannel, params: Params) => void;

    'messages': (request: { channelId: number, page: number }, params: Params) => void;
    'messages/new': (message: v1.INewMessage, params: Params) => void;
    'messages/edit': (message: v1.IEditMessage, params: Params) => void;
    'messages/delete': (message: v1.IDeleteMessage, params: Params) => void;
}

interface IConfig {
    host: string;
    port: number;
    namespace: string;
    token: string;
    userId: number;
}

class RPChatsAPIv1 extends EventEmitter<ResponseMap, RequestMap> {
    private readonly _socket: Socket<ResponseMap, RequestMap>;
    constructor(
        private readonly config: IConfig
    ) {
        super();

        // TODO: via config
        // const token = '4643_7c179652c5a9e73e9d3c2f296a3db1f1';
        // const userId = 4643;

        this._socket = io(
            this._generateConnectionURL(),
            { autoConnect: false, query: { token: config.token, user_id: config.userId + "" }, transports: ['websocket'] }
        );

        this._socket.on('internal-error', (error: string, params: Params) => this.emit('internal-error', error, params));

        this._socket.on('chats', (chats: v1.IChat[], params: Params) => this.emit('chats', chats, params));
        this._socket.on('chats/describe', (chat: v1.IChatDetails, params: Params) => this.emit('chats/describe', chat, params));
        this._socket.on('chats/new', (chat: v1.IChat, params: Params) => this.emit('chats/new', chat, params));
        this._socket.on('chats/edit', (chat: v1.IChat, params: Params) => this.emit('chats/edit', chat, params));
        this._socket.on('chats/delete', (chat: v1.IChat, params: Params) => this.emit('chats/delete', chat, params));

        this._socket.on('channels', (channels: v1.IChannel[], params: Params) => this.emit('channels', channels, params));
        this._socket.on('channels/describe', (channel: v1.IChannelDetails, params: Params) => this.emit('channels/describe', channel, params));
        this._socket.on('channels/members', (members: v1.IChannelMember[], params: Params) => this.emit('channels/members', members, params));
        this._socket.on('channels/new', (channel: v1.IChannel, params: Params) => this.emit('channels/new', channel, params));
        this._socket.on('channels/edit', (channel: v1.IChannel, params: Params) => this.emit('channels/edit', channel, params));
        this._socket.on('channels/delete', (channel: v1.IChannel, params: Params) => this.emit('channels/delete', channel, params));

        this._socket.on('messages', (messages: v1.IMessage[], page: number, params: Params) => this.emit('messages', messages, page, params));
        this._socket.on('messages/new', (message: v1.IMessage, params: Params) => this.emit('messages/new', message, params));
        this._socket.on('messages/edit', (message: v1.IMessage, params: Params) => this.emit('messages/edit', message, params));
        this._socket.on('messages/delete', (message: v1.IMessage, params: Params) => this.emit('messages/delete', message, params));
    }

    async init() {
        // TODO: add timeout
        // TODO: handle several calls
        return new Promise<void>((resolve, reject) => {
            this._socket.once('initialized', () => {
                this._socket.off('connect_error', reject);
                this._socket.off('error', reject);

                this._socket.on('connect_error', this._onError);
                this._socket.on('error', this._onError);
                this._socket.on('disconnect', this._onDisconnect);

                return resolve();
            });

            this._socket.once('connect_error', reject);
            this._socket.once('error', reject);

            this._socket.connect();
        });
    }

    async deinit() {
        // TODO: add timeout
        // TODO: handle several calls
        return new Promise<void>((resolve, reject) => {
            this._socket.off('connect_error', this._onError);
            this._socket.off('error', this._onError);
            this._socket.off('disconnect', this._onDisconnect);

            this._socket.once('disconnect', () => {
                this._socket.off('error', reject);

                return resolve();
            });
            this._socket.once('error', reject);

            this._socket.disconnect();
        });
    }

    chats(): MessageId {
        const messageId = uuid();

        console.log('chats', messageId, this._socket.id, this._socket.connected);
        this._socket.emit('chats', { messageId });

        return messageId;
    }

    chatsDescribe(chatId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/describe', chatId, { messageId });

        return messageId;
    }

    chatsNew(newChat: v1.INewChat): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/new', newChat, { messageId });

        return messageId;
    }

    chatsEdit(editChat: v1.IEditChat): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/edit', editChat, { messageId });

        return messageId;
    }

    chatsDelete(deleteChat: v1.IDeleteChat): MessageId {
        const messageId = uuid();

        this._socket.emit('chats/delete', deleteChat, { messageId });

        return messageId;
    }

    channels(): MessageId {
        const messageId = uuid();

        this._socket.emit('channels', { messageId });

        return messageId;
    }

    channelsDescribe(channelId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/describe', channelId, { messageId });

        return messageId;
    }

    channelsMembers(channelId: number): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/members', channelId, { messageId });

        return messageId;
    }

    channelsMembersEdit(channelId: number, membersToAdd: number[], membersToRemove: number[]): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/members/edit', channelId, membersToAdd, membersToRemove, { messageId });

        return messageId;
    }

    channelsNew(newChannel: v1.INewChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/new', newChannel, { messageId });

        return messageId;
    }

    channelsEdit(editChannel: v1.IEditChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/edit', editChannel, { messageId });

        return messageId;
    }

    channelsDelete(deleteChannel: v1.IDeleteChannel): MessageId {
        const messageId = uuid();

        this._socket.emit('channels/delete', deleteChannel, { messageId });

        return messageId;
    }

    messages(request: { channelId: number, page: number }): MessageId {
        const messageId = uuid();

        this._socket.emit('messages', request, { messageId });

        return messageId;
    }

    messagesNew(newMessage: v1.INewMessage): MessageId {
        const messageId = uuid();

        this._socket.emit('messages/new', newMessage, { messageId });

        return messageId;
    }

    messagesEdit(editMessage: v1.IEditMessage): MessageId {
        const messageId = uuid();

        this._socket.emit('messages/edit', editMessage, { messageId });

        return messageId;
    }

    messagesDelete(deleteMessage: v1.IDeleteMessage): MessageId {
        const messageId = uuid();

        this._socket.emit('messages/delete', deleteMessage, { messageId });

        return messageId;
    }

    private _onError = (error: Error) => {
        this.emit('error', error);
    }

    private _onDisconnect = _ => {
        this._socket.connect();
    };

    private _generateConnectionURL() {
        return `${this.config.host}:${this.config.port}/${this.config.namespace}`;
    }
}

export { RPChatsAPIv1 };
