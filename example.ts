import RPChats from './src';

main()
    .catch(_ => console.log('Error: ', _))
    // @ts-ignore
    .then(_ => process.exit(0));

async function main() {
    const rpChats = new RPChats({
        host: 'http://3.143.71.225',
        port: 3000
    });
    
    await rpChats.init();

    console.log('inited!');

    const chats = await rpChats.chatsAsync();

    console.log('chats: ', chats);
}
